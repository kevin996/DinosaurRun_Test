package dinosaur;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.Rectangle;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Kevin
 * 恐龙运动线程，负责恐龙的运动轨迹(走、跳、蹲)以及
 * 更新恐龙的坐标
 * */

public class DinosaurThread extends Thread{

    //    获得恐龙的图片
    protected GamePane pane;
    protected BufferedImage[] dinoImg;
    protected BufferedImage image;

    protected int fresh = DinosaurMain.FRESH;//刷新时间10ms
    protected static long millis = 4;//睡眠时间变化 4 -> 3 -> 2
    protected int time = 0;//计时器
    protected int recordJump = 0;
    protected int acceleration = 2;//重力加速度
    protected int speed = -INITSPEED;

//    键盘判断跳跃、蹲下
    protected boolean moveJudge = false;
    protected boolean jumpJudge = true;
    protected boolean downJudge = false;
    protected boolean setSpeedDrop = false;

    //    恐龙的坐标
    private int dinoX;
    private int dinoY;

    //    更新x、y的坐标,x、y的坐标传入到了画板中
    private int x;
    private int y;

    /**
     * 可修改的3个常量
     */
    public static final int LOWER_Y = 440;//最低的y轴坐标
    private static final int HIGH_JUMP = 210;//能跳跃的最大高度
    private static final int INITSPEED = 28;//初始速度

    //    constructor
    public DinosaurThread() { }

    public DinosaurThread(BufferedImage[] dinoImg, GamePane gamePane) {
        this.dinoImg = dinoImg;
        this.image = dinoImg[0];
        this.pane = gamePane;
        this.x = 50;
        this.y = LOWER_Y;
        this.dinoY = this.y;
    }

    //    判断碰撞时用
    private int dinoHeight;
    private int dinoWidth;

    //    getters
    public int getDinoHeight() {
        dinoHeight = image.getHeight();
        return dinoHeight;
    }

    public int getDinoWidth() {
        dinoWidth = image.getWidth();
        return dinoWidth;
    }

    public int getX() { return x; }

    public int getY() { return y; }

    public static int getHighJump() {
        return HIGH_JUMP;
    }

    public static int getINITSPEED() {
        return INITSPEED;
    }

    public static int getLowerY() {
        return LOWER_Y;
    }

    public BufferedImage getImage() {
        return image;
    }

    public static long getMillis() {
        return millis;
    }

    //    setters
    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public static void setMillis(long millis) {
        DinosaurThread.millis = millis;
    }

    //    methods
    public void isMoveJudge() {
        this.moveJudge = true;
        this.jumpJudge = false;
        this.downJudge = false;
    }
    public void isJumpJudge(){
        this.jumpJudge = true;
        this.downJudge = false;
        this.moveJudge = false;
    }
    public void isDownJudge(){
        this.downJudge = true;
        this.jumpJudge = false;
        this.moveJudge = false;
    }
    public void releaseDown(){
        y = LOWER_Y;
        System.out.println("蹲下已完成，图片复位");
        isMoveJudge();
        System.out.println("恢复成站立姿态");
        recordJump = 0;
        speed = - INITSPEED;
        System.out.println("蹲下判断"+downJudge+"跳跃判断"+jumpJudge+"移动判断"+moveJudge);
    }

//    Rectangle methods
    public Rectangle boundDino(){
//        整个恐龙的矩形
        return new Rectangle(getX(),getY(),getDinoWidth(),getDinoHeight());
    }

    public Rectangle boundDinoHead(){
//        切割成一个31×44的头部矩形
        return new Rectangle(getX()+getDinoWidth()/2, getY()+5,getDinoWidth()/2-6,getDinoHeight()/3);
    }

    public Rectangle boundDinoBody(){
//        切割成一个31×58的身体矩形
        return new Rectangle(getX()+8,getY()+getDinoHeight()/3+2, getDinoWidth()-28,getDinoHeight()/3);
    }

    public Rectangle boundDinoFeet(){
//        切割成一个28×28的脚矩形
        return new Rectangle(getX()+24,getY()+63,28,28);
    }

    public Rectangle boundDinoDown(){
        return new Rectangle(getX()+1,getY()+1,getDinoWidth()-12,getDinoHeight()/3*2);
    }
}