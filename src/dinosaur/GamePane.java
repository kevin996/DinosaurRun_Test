package dinosaur;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Grey
 * 游戏面板类，继承自JPanel类
 */

public class GamePane extends JPanel implements KeyListener {

    static final int WINDOW_WIDTH = 1280;
    static final int WINDOW_HEIGHT = 720;
    static final int FULL_WINDOW_WIDTH = 1920;
    public static final Color SKY_COLOR = new Color(193, 233, 255);

    private Cloud cloud;
    private Ground ground;
    private Cactus cactus, cactus2;
    private DinosaurThread dino;
    private Bird bird;
    DinosaurAction dinoAction = new DinosaurAction();
    AudioControl audioControl = new AudioControl();
    private JLabel scoreLabel;
    private Font pixelFont;
    private ScoreThread sc;
    private static BufferedImage sunImg;
    private int cloud_x = WINDOW_WIDTH + 300;
    private String folderName = "/colored/";
    JLabel tips = new JLabel(
            "[↑][↓]: Jump and down    " +
                    "[Enter]: Pause & Resume    " +
                    "[P]: Score-board    " +
                    "[ESC]: Exit game    ");
    JFrame mainFrame;

    GamePane() {
        //this.setLayout(null);
        pixelFont = new Font(null, Font.PLAIN, 30);
        setPixelFont();
        scoreLabel = new JLabel();
        scoreLabel.setLocation(50, 510);
        scoreLabel.setHorizontalAlignment(SwingConstants.CENTER);
        scoreLabel.setOpaque(true);
        scoreLabel.setFont(pixelFont);
        scoreLabel.setForeground(Color.darkGray);
        tips.setFont(new Font("Heiti", Font.PLAIN, 20));
        tips.setHorizontalAlignment(SwingConstants.CENTER);
        GridLayout grid = new GridLayout(15, 1);
        //grid.setHgap(30);
        this.setLayout(grid);
        this.add(scoreLabel);
        this.add(tips);
    }

    public void setMainFrame(JFrame frame) {
        this.mainFrame = frame;
    }

    public void setTheme(int choice) {
        if (choice == DinosaurMain.WHITE_STYLE) {
            folderName = "/2x/";
            scoreLabel.setBackground(Color.WHITE);
            setBackground(Color.WHITE);
        } else if (choice == DinosaurMain.COLOR_STYLE) {
            folderName = "/colored/";
            scoreLabel.setBackground(SKY_COLOR);
            setBackground(SKY_COLOR);
        }
        try {
            sunImg = ImageIO.read(GamePane.class.getResource("/images" + folderName + "round_sun_moon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }//太阳
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        paintSun(g);
        paintGround(g);
        paintCactus(g);
        if (cactus2 != null)
            paintCactus2(g);
        paintCloud(g);
        paintDino(g);
        paintBird(g);
        paintScore();
    }

    public void setDino(DinosaurThread dino) {
        this.dino = dino;
    }

    public void setCloud(Cloud cloud) {
        this.cloud = cloud;
    }

    public void setGround(Ground ground) {
        this.ground = ground;
    }

    public void setCactus(Cactus cactus) {
        this.cactus = cactus;
    }

    public void setCactus2(Cactus cactus) {
        this.cactus2 = cactus;
    }

    public void setBird(Bird bird) {
        this.bird = bird;
    }

    public void paintCloud(Graphics g) {
//        if (this.cloud_x <-cloud.getImage().getWidth())
//            cloud_x = WINDOW_WIDTH;
        g.drawImage(cloud.getImage(), cloud.getX(), cloud.getY(), null);
//        g.drawImage(cloud.getImage(), cloud_x--, cloud.getY() + 30, null);
    }

    public void paintDino(Graphics g) {
        g.drawImage(dino.getImage(), dino.getX(), dino.getY(), null);
    }

    public void paintGround(Graphics g) {
        g.drawImage(ground.getImage1(), ground.x1, ground.y1, null);
        g.drawImage(ground.getImage2(), ground.x2, ground.y2, null);
    }

    public void paintCactus(Graphics g) {
        g.drawImage(cactus.getTempImage(), cactus.getX(), cactus.getY(), null);
    }

    public void paintCactus2(Graphics g) {
        g.drawImage(cactus2.getTempImage(), cactus2.getX(), cactus2.getY(), null);
    }

    public void paintBird(Graphics g) {
        g.drawImage(bird.getTempImage(), bird.getX(), bird.getY(), null);
    }

    public void markScore(ScoreThread sc) {
        this.sc = sc;
    }

    private void paintSun(Graphics g) {
//        g.drawImage(sunImg, WINDOW_WIDTH / 10 * 8, WINDOW_HEIGHT / 10 * 8, null);//太阳
        g.drawImage(sunImg, 1060, 80, null);//太阳
    }


    //更新分数
    public void paintScore() {
        if (DinosaurMain.state != DinosaurMain.PLAYING)
            return;
        scoreLabel.setText(String.format("SCORE: %.0f | HI-SCORE: %.0f\n", sc.getScore(), sc.getHighScore()));
    }

    /**
     * 使用自定义的像素风格字体
     * 从外部导入字体，并创建缓存
     */
    /* 参考自：http://www.pianshen.com/article/8012101105/ */
    public void setPixelFont() {
        InputStream is;
        BufferedInputStream bis;
        try {
            is = GamePane.class.getResourceAsStream("/fonts/pixel_mono.otf");
            bis = new BufferedInputStream(is);
            // createFont返回一个使用指定字体类型和输入数据的新 Font。
            // 新 Font磅值为 1，样式为 PLAIN,注意 此方法不会关闭 InputStream
            pixelFont = Font.createFont(Font.TRUETYPE_FONT, bis);
            pixelFont = pixelFont.deriveFont(Font.PLAIN, 30);
            is.close();
            bis.close();
        } catch (FontFormatException | IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void keyTyped(KeyEvent e) {
        //不用管这个
    }

    @Override
    public void keyPressed(KeyEvent e) {
        //按键按下处理的事件
        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP: //上箭头
                dino.isJumpJudge();
                audioControl.jumpAudio();
//                System.out.println("SPACE or UP Pressed.");
                break;
            case KeyEvent.VK_DOWN: //下箭头
                if (dino.jumpJudge) {
                    //当恐龙在跳跃时按下down
                    dino.isDownJudge();//先将状态设置为down
                    dinoAction.setSpeedDrop();
                } else {
                    dino.isDownJudge();
                }
//                System.out.println("DOWN Pressed.");
                break;
            case KeyEvent.VK_ENTER: {
                //改成Enter键，暂停与开始
//                System.out.println("Enter Pressed.");
                if (DinosaurMain.state == DinosaurMain.PLAYING) {
                    DinosaurMain.setGameState(DinosaurMain.PAUSE);
                } else {
                    DinosaurMain.setGameState(DinosaurMain.PLAYING);
                }
                break;
            }
            case KeyEvent.VK_P: {
                sc.showScoreBoard();
                break;
            }
            case KeyEvent.VK_ESCAPE: {
                mainFrame.dispose();
                break;
            }
            default:
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_DOWN://下箭头
//                System.out.println("松开down键后jumpJudge:" + dino.jumpJudge);
                dino.releaseDown();
//                System.out.println("DOWN released");
                break;
            default:
                break;
        }
    }

    /*
    public void restart(){
        dino.setX(50);
        dino.setY(DinosaurThread.getLowerY());
        paint(getGraphics());
        sc.setScore(0);
        DinosaurMain.setGameState(DinosaurMain.PLAYING);
    }
     */
}

