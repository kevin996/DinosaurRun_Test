package dinosaur;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin
 * 各种文件处理，分数面板
 */

public class ScoreBoard extends JFrame   {
    private double highestScore = 0;
    private String time;

    public double getHighestScore() {
        return highestScore;
    }

    public String getTime() {
        return time;
    }

    public void setHighestScore(double highestScore) {
        this.highestScore = highestScore;
    }

    public void setTime(String time) {
        this.time = time;
    }

    //    创建分数文件
    public boolean createFile(File file) {
        boolean result = false;

        if (!file.exists()) {
            try {
                result = file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    //    读文本
    public List<String> readFile(File file) {
        if (!file.exists() || !file.isFile()) {
            return null;
        }

        List<String> content = new ArrayList<String>();
        try (FileInputStream fileInputStream = new FileInputStream(file);
             InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "GBK");
             BufferedReader reader = new BufferedReader(inputStreamReader)){
            String lineContent = "";
            while ((lineContent = reader.readLine()) != null) {
                content.add(lineContent);
//                System.out.println(lineContent);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    //    写文本
    public void writeFile(File file, String content) throws IOException {
        synchronized (file) {
            FileWriter fileWriter = new FileWriter(file, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(content + "\n");
            bufferedWriter.flush();
            bufferedWriter.close();
        }
    }

}
