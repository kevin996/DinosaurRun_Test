package dinosaur;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * @author Grey
 * 云朵类，继承自背景线程类
 */

public class Cloud extends BackgroundThread implements Runnable {

    public Cloud(BufferedImage image, GamePane pane) {
        super(image, pane);
        setX(GamePane.WINDOW_WIDTH-300);
        setY(150);
        super.setMoveSpeed(1); //设置云朵的移动速度
    }

    @Override
    public void run() {
        while (DinosaurMain.state != DinosaurMain.GAME_OVER) {
            if (DinosaurMain.state == DinosaurMain.PLAYING)
                roll();
            pane.repaint();
        }
    }

    @Override
    public void roll() {
        //System.out.println("Cloud thread running..");
        if (getX() < getImage().getWidth() * -1) {
            //如果云已经过去了，那就等一会再来
            try {
                sleep(950 * r.nextInt(4)); //等待的时间（毫秒）间隔在区间(950,3800]上。
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            setX(GamePane.WINDOW_WIDTH); // 之后依然从窗口边缘进入
        } else {
            setX(getX() - DinosaurMain.speed / 4);
        }
        pane.repaint();
        try {
            sleep(getSleepTime() * 3);//降低云的运动速度
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
