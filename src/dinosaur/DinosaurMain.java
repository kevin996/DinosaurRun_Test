package dinosaur;

import com.sun.xml.internal.bind.v2.TODO;

import java.awt.*;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;
import java.util.Timer;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.plaf.FontUIResource;

/**
 * 整个程序的主线程，控制整个游戏进行
 * 同时负责监听键盘事件
 */

public class DinosaurMain extends JFrame {

    //版本号
    private static final String CUR_VERSION = "beta-1.0";
    public static final int COLOR_STYLE = 0;
    public static final int WHITE_STYLE = 1;

    //窗口长宽
    public static final int SCALE = 2; //缩放比例，默认使用colored的
    public static final int HEIGHT = GamePane.WINDOW_HEIGHT;
    public static final int WIDTH = GamePane.WINDOW_WIDTH;

    //游戏运行状态
    public static final int START = 0;
    public static final int PLAYING = 1;
    public static final int PAUSE = 2;
    public static final int GAME_OVER = 3;
    public static int state = START;

    static final int FRESH = 10;//刷新时间
    static int speed = 4;// 移动速度，全局变量，后期可以使它随时间变化
    static int period = 1500; //间隔时间
    public static int score = 0; //得分
    public static int hightScore = 0; //得分

    private Timer timer; //定时器
    private static Random random = new Random();

    //保存图片
    private static BufferedImage[] cactusImg; //仙人掌，组
    private static BufferedImage birdImg1, birdImg2; //鸟儿的两个形态
    private static BufferedImage[] dinoImg; //会用到，主角
    private static BufferedImage[] groundImage;//地平线
    private static BufferedImage cloudImg;//云朵

    // 静态初始化器，加载图片和资源等
    static {
        /*加载图片，如背景、小恐龙等*/
        try {
            System.out.println("Loading dino...");
            dinoImg = new BufferedImage[]{
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/dino_1.png")),
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/dino_2.png")),
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/dino_3.png")),
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/dino_4.png")),
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/dino_5.png")),
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/dino_6.png")),
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/dino_down_1.png")),
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/dino_down_2.png")),
            };
            System.out.println("Loading ground...");
            System.out.println("Loading groundImage...");
            groundImage = new BufferedImage[]{
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/ground.png")),
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/ground.png")),
            };
            System.out.println("Loading clouds & sunImg...");
            cloudImg = ImageIO.read(DinosaurMain.class.getResource("/images/colored/cloud_long.png"));
            System.out.println("Loading barriers(cactus & birds)...");
            cactusImg = new BufferedImage[]{
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/cactus_1.png")),
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/cactus_3.png")),
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/cactus_5.png")),
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/cactus_6.png")),
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/giant_cactus_1.png")),
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/giant_cactus_2.png")),
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/giant_cactus_3.png")),
                    ImageIO.read(DinosaurMain.class.getResource("/images/colored/giant_cactus_group.png")),
            };
            birdImg1 = ImageIO.read(DinosaurMain.class.getResource("/images/colored/bird_1.png"));
            birdImg2 = ImageIO.read(DinosaurMain.class.getResource("/images/colored/bird_2.png"));
            System.out.println("Loading resources finished.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static GamePane p;
    public static GameMenu menu = new GameMenu();

    //本类的构造方法
    public DinosaurMain() {
        /*窗口初始化操作*/
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // 默认关闭操作
        setTitle("Dino-game @ " + DinosaurMain.CUR_VERSION);
        setSize(WIDTH, HEIGHT); //设置窗口大小
        setLocation(328, 173); //确定显示位置
        setResizable(false);
        p = new GamePane();
        p.setTheme(COLOR_STYLE);
        add(p); //将游戏面板添加到JFrame中
        addKeyListener(p);
        p.setMainFrame(this);
    }

    public static void main(String[] args) throws InterruptedException {
//        int response = -1;
//        do{
//            //这里改为先启动菜单，在菜单里面启动游戏。
//            DinosaurMain game = new DinosaurMain();
//            game.play();
//            while (state != GAME_OVER){
//                game.threadSleep(300);
//            }
//            response = game.selectRestart();
//        }while (response==0);
        // TODO 如果不继续玩就返回游戏菜单面板 菜单面板包括简单模式/困难模式/分数榜
        menu.showMenu();
    }

    public void play() {
        setGameState(PLAYING);
        setVisible(true);

        //分数
        ScoreThread scoreThread = new ScoreThread();
        p.markScore(scoreThread);
        scoreThread.start();

        //小恐龙
        DinosaurThread dino = new DinosaurAction(dinoImg, p);//创建恐龙对象
        p.setDino(dino);
        dino.start();

        //云
        Cloud cloud = new Cloud(cloudImg, p); //创建云对象，并将面板传入
        p.setCloud(cloud);
        cloud.start();

        //地面
        Ground ground = new Ground(groundImage, p); //地面对象，并将面板传入
        p.setGround(ground);
        ground.start();

        //仙人掌
        Cactus cactus = new Cactus(cactusImg, p);
        p.setCactus(cactus);
        cactus.start();
        Cactus cactus2 = new Cactus(cactusImg, p, true);
        p.setCactus2(cactus2);
        cactus2.start();

        //鸟儿
        Bird bird = new Bird(birdImg1, birdImg2, p);
        p.setBird(bird);
        bird.start();

        //碰撞检测
        CrashThread crashThread = new CrashThread(cactus, cactus2, bird, dino, p, scoreThread);
        crashThread.start();

        System.out.println("Game started.");
    }

//    public void showGameOverMessage() {
//        ScoreThread sc = new ScoreThread();
//        int response;
//        Object[] options = {" 返回菜单 ", " 查看排行榜 "};
//        UIManager.put("OptionPane.messageFont", new FontUIResource(new Font(
//                "Microsoft Yahei", Font.BOLD, 18)));
//        response = JOptionPane.showOptionDialog(null, "     游戏结束\n ", " 游戏结束！ ", JOptionPane.DEFAULT_OPTION,
//                JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
//        if (response == 0) {
//            System.out.print("\n return menu\n");
//            dispose();
//        }
//        if (response == 1) {
//            System.out.print("\n check board\n");
//            sc.showScoreBoard();
//        }
//    }

    public void threadSleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int selectRestart() {
        int response;
        Object[] options = {" 确定 ", " 取消 "};
        UIManager.put("OptionPane.messageFont", new FontUIResource(new Font(
                "SimSun", Font.BOLD, 16)));
        response = JOptionPane.showOptionDialog(null, " 是否选择重新开始 ", " 您已阵亡 ", JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (response == 0) System.out.print("\nnow restart\n");
        if (response == 1) System.out.print("\nnow exit\n");
        return response;
    }

    public static void setGameState(int state) {
        DinosaurMain.state = state;
    }

    public static void setSpeed(int speed) {
        DinosaurMain.speed = speed;
    }

    public void setDoubleSpeed(int speed) {
        DinosaurMain.speed = speed * 2;
    }

    public void setNormalSpeed(int speed){
        DinosaurMain.speed = speed;
    }


}
