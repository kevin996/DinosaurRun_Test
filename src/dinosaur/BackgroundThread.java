package dinosaur;

import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * @author Grey
 * 背景运动类，继承自线程类，负责背景(云、地平线等)的运动
 */

public class BackgroundThread extends Thread {

    // 2019/11/2 这个类已经变为抽象类
    private int x, y;//坐标，必须为整型
    private BufferedImage image;
    private int moveSpeed; //移动速度，必须为整型
    protected GamePane pane; //传入的面板对象，用于在此面板上绘画（系统自动调用里面的paint()）
    private long sleepTime = 10; //线程休眠时长，必须为长整型

    protected Random r = new Random(); //产生随机数

    BackgroundThread() {
    }

    BackgroundThread(BufferedImage image, GamePane pane) {
        this.image = image;
        this.pane = pane;
    }

    public void setToPane(GamePane pane) {
        this.pane = pane;
    }

    @Override
    public void run() {}

    public void roll() {}

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public void setPane(GamePane pane) {
        this.pane = pane;
    }

    public BufferedImage getImage() {
        return image;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setMoveSpeed(int moveSpeed) {
        this.moveSpeed = moveSpeed;
    }

    public int getMoveSpeed() {
        return moveSpeed;
    }

    public void setSleepTime(long sleepTime) {
        this.sleepTime = sleepTime;
    }

    public long getSleepTime() {
        return sleepTime;
    }

    public void threadSleep(long millis){
        try {
            Thread.sleep(millis);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
