package dinosaur;

import java.awt.image.BufferedImage;

/**
 * @author Kevin
 * 小恐龙的运动轨迹
 */

public class DinosaurAction extends DinosaurThread {

    GameMenu menu;

    public DinosaurAction() {

    }

    public DinosaurAction(BufferedImage[] dinoImg, GamePane gamePane) {
//        构造方法，继承父类
        super(dinoImg, gamePane);
    }

    public void setMenu(GameMenu menu){
        this.menu = menu;
    }


    @Override
    public void run() {
        while (DinosaurMain.state != DinosaurMain.GAME_OVER) {
            if (DinosaurMain.state == DinosaurMain.PLAYING)
                move();
            pane.repaint();
        }
        DinosaurMain.menu.showGameOverMessage();
    }

    //    小恐龙移动
    public void move() {
        if (moveJudge && !jumpJudge && !downJudge) {
            /**
             *   交替步伐
             */
            time = time >= (100 * 2 * 2) ? 0 : time;//防止溢出
            int temp = time / (100 * 2) % 2;//time每计算20次换一张
            switch (temp) {
                case 0:
                    image = dinoImg[2];//第三张
                    threadSleep(millis);
                    break;
                case 1:
                    image = dinoImg[3];//第四张
                    threadSleep(millis);
                    break;
            }
            time += fresh;

        } else if (jumpJudge && !moveJudge && !downJudge) {
            /**
             * 跳跃
             */
            image = dinoImg[0];//第一张
//            System.out.println("recordJump:"+recordJump);
            if (recordJump < getHighJump()) {
//                System.out.println("往上跳");
                if (jumpJudge) {
                    setY(getY() + speed);
                }//多加判断,否则在跳跃时按down后，可能会
                recordJump -= speed;
                speed += acceleration;//速度先减到0
                threadSleep(36 + speed);
            } else {
//                System.out.println("往下降");
                if (recordJump == getHighJump()) threadSleep(30);//停住一会
                speed += acceleration;
                recordJump += speed;
                if (getY() < getLowerY()) {
                    /**
                     *多加判断是否还在跳跃状态,否则在跳跃时按down后，可能会出现
                     *down完成后，setY还会再往下执行一次
                     **/
                    if (jumpJudge) {
                        setY(getY() + speed);
                    }

                    threadSleep(36 - speed);
                } else {
                    isMoveJudge();
                    recordJump = 0;
                    speed = -getINITSPEED();
                    //threadSleep(100);
                }
            }
        } else if (downJudge && !jumpJudge && !moveJudge) {
            /**
             * 蹲下
             */
            setY(getLowerY() + 38);
            time = time >= (100 * 2 * 2) ? 0 : time;//防止溢出
            int temp = time / (100 * 2) % 2;//每×s换一张
            switch (temp) {
                case 0:
                    image = dinoImg[6];//第一张
                    threadSleep(millis);
                    break;
                case 1:
                    image = dinoImg[7];//第三张
                    threadSleep(millis);
                    break;
            }
            time += fresh;
        }
    }

    public void setSpeedDrop() {
        System.out.println("执行setSpeedDrop");
        while (getY() < getLowerY() && getY() + Math.abs(speed) < getLowerY()) {
            setY(getY() + Math.abs(speed));
        }
        setY(getLowerY() + 38);
        recordJump = 0;//bug之一，没有初始化为0会导致下次up键失败
        speed = -getINITSPEED();
    }

    public void threadSleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    /*
    public void setSpeedDrop(){
        if (setSpeedDrop){
            System.out.println("carry setSpeedDrop");
            System.out.println(getY());
            if (getY() < getLowerY()) {
                count += acceleration;
                if (getY() + count <= getLowerY()) {
                    setY(getY() + count);
                    System.out.println(getY());
                    try {
                        Thread.sleep(32-count);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    setY(getLowerY());
                }
            }else {
                count = 0;
                jumpJudge = false;
                setSpeedDrop = false;
            }
        }
    }

     */
}
