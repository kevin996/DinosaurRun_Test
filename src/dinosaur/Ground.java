package dinosaur;

import java.awt.image.BufferedImage;

/**
 * @author Grey
 * 地平线类，继承自背景线程类
 * 思路：用两张图拼接起来，一前一后一直滚动，前边的滚完了就补去后边
 */

public class Ground extends BackgroundThread {

    private BufferedImage image1, image2;
    int x1, y1, x2, y2;

    public Ground(BufferedImage[] images, GamePane pane) {
        this.image1 = images[0];
        this.image2 = images[1];
        this.pane = pane;
        setSleepTime(10);
        x1 = 0;
        y1 = GamePane.WINDOW_HEIGHT / 10 * 7;
        x2 = x1 + image1.getWidth(); //图二放在图一后面，实现一直循环
        y2 = GamePane.WINDOW_HEIGHT / 10 * 7;
    }

    @Override
    public void run() {
        while (DinosaurMain.state != DinosaurMain.GAME_OVER){
            if(DinosaurMain.state == DinosaurMain.PLAYING)
                roll();
            pane.repaint();
        }
    }

    @Override
    public void roll() {
        //System.out.println("Ground thread running..");
        //如果尾巴走过了就恢复
        if (x1 < image1.getWidth() * -1) {
            x1 = GamePane.FULL_WINDOW_WIDTH;
        }

        //后面的依然跟在前面
        if (x2 < image2.getWidth() * -1) {
            x2 = x1 + image2.getWidth();
        }

        x1 -= DinosaurMain.speed;
        x2 -= DinosaurMain.speed;

        pane.repaint();

        try {
            sleep(getSleepTime()); //线程休眠刷新时间间隔，默认2000ms
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public BufferedImage getImage1() {
        return image1;
    }

    public BufferedImage getImage2() {
        return image2;
    }

}
