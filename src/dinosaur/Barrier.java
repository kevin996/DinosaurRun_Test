package dinosaur;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.Rectangle;
import java.util.Random;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Grey
 * 障碍物类，继承自线程类
 */

public class Barrier extends Thread {

    protected int x, y; //坐标，必须为整型

    private BufferedImage tempImage;
    private boolean crashed; //是否碰撞
    protected int speed; //坐标的移动速度，必须为整型
    protected GamePane pane; //传入的面板对象，用于在此面板上绘画（系统自动调用里面的paint()）
    private long sleepTime = 2000; //线程休眠时长，必须为长整型

    protected Timer timer;
    protected Random r = new Random(); //产生随机数

    public Barrier() {
    }

    public Barrier(BufferedImage image, GamePane pane) {
        this.tempImage = image;
        this.pane = pane;
        crashed = false;
    }

    public void setToPane(GamePane pane) {
        this.pane = pane;
    }

    @Override
    public void run() {
    }


    public boolean isCrashed() {
        return crashed;
    }

    public void setCrashed(boolean crashed) {
        this.crashed = crashed;
    }

    public void roll() {

    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public BufferedImage getTempImage() {
        return tempImage;
    }

    public void setTempImage(BufferedImage tempImage) {
        this.tempImage = tempImage;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public long getSleepTime() {
        return sleepTime;
    }

    public int getBarryWidth(){
        return tempImage.getWidth();
    }

    public int getBarryHeigh(){
        return tempImage.getHeight();
    }

    public void threadSleep(long millis){
        try {
            Thread.sleep(millis);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

}
