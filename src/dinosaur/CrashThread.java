package dinosaur;

import java.io.IOException;

public class CrashThread extends Thread{
//    碰撞事件检测线程，当恐龙与障碍物碰撞，游戏结束
    /**
     * @author Kevin
     * 碰撞检测思路
     *1.使用Shape.intersects(Rectangle2D r)方法，原理是Shape的内部区域与矩形的内部区域相交，
     * 或者相交的可能性很大且执行计算的代价太高，则返回true；
     * 2.使用Shape.intersects(double x,double y,double w,double h)
     * x - 指定矩形区域左上角的 X 坐标; y - 指定矩形区域左上角的 Y 坐标;
     * w - 指定矩形区域的宽度; h - 指定矩形区域的高度
     */

    GamePane pane;
    Cactus cactus1,cactus2;
    Bird bird;
    DinosaurThread dino;
    ScoreThread scoreThread;
    AudioControl audioControl = new AudioControl();

    CrashThread(Cactus cactus1, Cactus cactus2, Bird bird, DinosaurThread dino,GamePane pane,ScoreThread scoreThread) {
        this.cactus1 = cactus1;
        this.cactus2 = cactus2;
        this.bird = bird;
        this.dino = dino;
        this.pane = pane;
        this.scoreThread = scoreThread;
    }

    @Override
    public void run() {
        while (DinosaurMain.state != DinosaurMain.GAME_OVER) {
            if (DinosaurMain.state == DinosaurMain.PLAYING) {
                try {
                    crashJudge();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            pane.repaint();
        }
    }

    public void crashJudge() throws IOException {
        cactus1.boundCactus();
        cactus2.boundCactus();
        bird.boundBird();
//        System.out.println(cactus1.boundCactus());
//        System.out.println(cactus2.boundCactus());
//        System.out.println(bird.boundBird());

        if (!dino.downJudge && (cactus1.boundCactus().intersects(dino.boundDino()) ||
        cactus2.boundCactus().intersects(dino.boundDino())) ){
            if (cactus1.boundCactusTrunk().intersects(dino.boundDinoHead()) ||
                    cactus1.boundCactusTrunk().intersects(dino.boundDinoBody()) ||
                    cactus1.boundCactusTrunk().intersects(dino.boundDinoFeet()) ||
                    cactus1.boundCactusBody().intersects(dino.boundDinoHead()) ||
                    cactus1.boundCactusBody().intersects(dino.boundDinoBody()) ||
                    cactus1.boundCactusBody().intersects(dino.boundDinoFeet()) ||
                    cactus2.boundCactusTrunk().intersects(dino.boundDinoHead()) ||
                    cactus2.boundCactusTrunk().intersects(dino.boundDinoBody()) ||
                    cactus2.boundCactusTrunk().intersects(dino.boundDinoFeet()) ||
                    cactus2.boundCactusBody().intersects(dino.boundDinoHead()) ||
                    cactus2.boundCactusBody().intersects(dino.boundDinoBody()) ||
                    cactus2.boundCactusBody().intersects(dino.boundDinoFeet())){
                System.out.println("dino:"+dino.boundDino());
                System.out.println("cactus1:"+cactus1.boundCactus());
                System.out.println("cactus2:"+cactus2.boundCactus());
                Game_over();
            }
        }
        if (!dino.downJudge && (bird.boundBird().intersects(dino.boundDino())) ){
            if (bird.boundBirdHead().intersects(dino.boundDinoHead()) ||
                    bird.boundBirdHead().intersects(dino.boundDinoBody()) ||
                    bird.boundBirdHead().intersects(dino.boundDinoFeet()) ||
                    bird.boundBirdBody().intersects(dino.boundDinoFeet()) ||
                    bird.boundBirdBody().intersects(dino.boundDinoHead())){
//                bird的身体与恐龙的身体不需要检测
                System.out.println("dino:"+dino.boundDino());
                System.out.println("bird:"+bird.boundBird());
                Game_over();
            }
        }
        if (dino.downJudge && (cactus1.boundCactus().intersects(dino.boundDino()) ||
        cactus2.boundCactus().intersects(dino.boundDino())) ){
            if (cactus1.boundCactusTrunk().intersects(dino.boundDinoDown()) ||
            cactus2.boundCactusBody().intersects(dino.boundDinoDown()) ||
            bird.boundBirdBody().intersects(dino.boundDinoDown())){
                System.out.println("dino:"+dino.boundDino());
                System.out.println("cactus1:"+cactus1.boundCactus());
                System.out.println("cactus2:"+cactus2.boundCactus());
                System.out.println("bird:"+bird.boundBird());
                Game_over();
            }
        }

        try {
            Thread.sleep(20);
        }catch (InterruptedException e){
            e.printStackTrace();
        }

    }

    public void Game_over() throws IOException {
        System.out.println("Game Over!!!");
        if (dino.downJudge){
            dino.setY(DinosaurThread.getLowerY());
            dino.setX(dino.getX()+15);
        }
        dino.setImage(dino.dinoImg[5]);
        DinosaurMain.setGameState(DinosaurMain.GAME_OVER);
        audioControl.hitAudio();
        scoreThread.recordRank();
        scoreThread.cancleTimer();
//        Thread.interrupted();
    }
}
