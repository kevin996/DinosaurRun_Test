package dinosaur;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameMenu extends JFrame implements ActionListener {

    private JButton easyModeButton;
    private JButton hardModeButton;
    private JButton checkButton;
    private JButton exitButton;
    JPanel menuPanel = new JPanel();
    GridLayout grid = new GridLayout(4, 1);
    private ScoreThread sc = new ScoreThread();
    ;
    private DinosaurMain game = new DinosaurMain();

    GameMenu() {
        init();
    }

    private void init() {

        Font buttonFont = new Font("Microsoft Yahei", Font.BOLD, 20);

        easyModeButton = new JButton("简单模式 / Easy Mode");
        easyModeButton.setFont(buttonFont);
        hardModeButton = new JButton("困难模式 / Hard Mode");
        hardModeButton.setFont(buttonFont);
        checkButton = new JButton("查看排行榜 / Score-board");
        checkButton.setFont(buttonFont);
        exitButton = new JButton("离开游戏 / Exit");
        exitButton.setFont(buttonFont);

        menuPanel.add(easyModeButton);
        menuPanel.add(hardModeButton);
        menuPanel.add(checkButton);
        menuPanel.add(exitButton);

        easyModeButton.addActionListener(this);
        hardModeButton.addActionListener(this);
        checkButton.addActionListener(this);
        exitButton.addActionListener(this);

        this.add(easyModeButton);
        this.add(hardModeButton);
        this.add(checkButton);
        this.add(exitButton);
        this.setLayout(grid);

        setSize(500, 600);
        setLocation(750, 300);
//        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("小恐龙冒险");
    }

    public void showMenu() {
        setVisible(true);
    }

    private void play() {
        game.play();

    }
    public void showGameOverMessage() {
        int response;
        Object[] options = {" 好的 ", " 查看排行榜 "};
        UIManager.put("OptionPane.messageFont", new FontUIResource(new Font(
                "Microsoft Yahei", Font.BOLD, 18)));
        response = JOptionPane.showOptionDialog(null, "    游戏结束\n ", " 游戏结束！ ", JOptionPane.DEFAULT_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
        if (response == 0) {
            System.out.println("ok");
//            dispose();
        }
        if (response == 1) {
            System.out.println("check board");
            sc.showScoreBoard();
        }
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        JButton button = (JButton) e.getSource();
        if (button == easyModeButton) {
            game.setNormalSpeed(5);
//            game.play();
            play();
        } else if (button == hardModeButton) {
            game.setDoubleSpeed(5);
            game.play();
        } else if (button == checkButton) {
            sc.showScoreBoard();
        } else if (button == exitButton) {
            System.exit(0);
        }
    }
}
