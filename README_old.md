# DinosaurRun_Test

## 公告
我们可以开始干活啦，分配一下任务：

- LH：负责音频类，分数统计类
- Kevin：负责恐龙类，碰撞判断类
- Grey：负责键盘事件（应该是放主类了），背景和障碍类
- 主类一起写

整个程序的UML类图和流程图可以 [点这里看](https://www.processon.com/view/link/5dadce49e4b06b7d6ef1d57d) ，密码是`gkd`。
>完整网址 https://www.processon.com/view/link/5dadce49e4b06b7d6ef1d57d

我们的思路整理在幕布 [点这里看](https://mubu.com/colla/5YUlk6TIYPu)
>完整网址 https://mubu.com/colla/5YUlk6TIYPu

## 一些约定：

1. 注重封装性，记得使用合适的修饰符
2. 一般来说写了多少就可以先提交多少，但提交前请请注意所写的东西要保证编译通过，再`commit`
3. `commit`时请尽量写清本次提交的更新内容
4. 每次`push`前记得先`pull`再`push`
5. 有问题及时沟通😃
6. `取消空格跳跃!!`
   `取消空格跳跃!!`
   `取消空格跳跃!!`

## 一些难点

### 碰撞检测
>- 使用Shape.intersects(Rectangle2D r)方法，原理是Shape的内部区域与矩形的
内部区域相交，或者相交的可能性很大且执行计算的代价太高，则返回true；
>- 使用Shape.intersects(double x,double y,double w,double h),
x - 指定矩形区域左上角的 X 坐标; y - 指定矩形区域左上角的 Y 坐标;
w - 指定矩形区域的宽度; h - 指定矩形区域的高度

### 磨人的空指针问题
> 初始化很重要

### 重新开始后定时器问题
> 由于一开始并没有在GAME_OVER后结束掉timer，会使得每次restart就增加一个timer，故会出现重复加速的现象
> **解决方法**
>
> 新增包含timer.cancel()的方法以供调用即可

### 导出jar包后没有声音
> - 在项目中建文件夹sound，不是在SRC下建包，音频文件（a.wav)放在sound下
> - String path=new URL("file:sound//a.wav").getPath();
> - 生成jar包后，把sound文件夹拷贝到jar包同一个文件夹，运行即可完成。


## 参考资料

- [1] 陈国君. Java程序设计基础（第五版）[M]. 北京：清华大学出版社，2015年. (199-217, 248-266, 346-347)

- [2] 罗杰·杜德勒. Git简易使用指南. Bootstrap中文网，https://www.bootcss.com/p/git-guide/

- [3] javatuanzhang. Java打飞机小游戏. cnblogs，https://www.cnblogs.com/java1024/p/7985173.html
- [4] 子耶. Java多线程实现简单动画（小球运动）效果. CSDN，https://blog.csdn.net/qq_36962569/article/details/80629927


~~三个JavaFX的教程，各取所需：~~

~~- JavaFX - w3cschool: https://www.w3cschool.cn/java/javafx-tempImage-imageview.html~~

~~- JavaFX中文资料: http://www.javafxchina.net/blog/docs/tutorial1/~~

~~- JavaFX教程 （中文） - code.makery: https://code.makery.ch/zh-cn/library/javafx-tutorial/~~

已改用AWT绘制界面，放弃极为先进但不适合我们的JavaFX。:(

---


### 附：参考文献格式

#### 专著（书籍）[M]  
[序号]主要作者. 文献题名[M]. 出版地：出版社，出版年. 参考页码（有些需要，有些不需要标注） 

>例如：   
>[1] 朱敛，曹凯鸣，周润琦等. 生物化学实验. 上海：科学技术出版社，1981.（12-15）


#### （网络文献）期刊或电子期刊  

[1] Anna C. McFadden. College Students' Use of the Internet. Education Policy Analysis Archives. Volume  7 Number 6. [Online] Available:  http://epaa.asu.edu/epaa/v7n6.html (April 17,2000)

[2] 符福桓. 关于信息管理学学科建设与发展的思考(一).《中国信息导报》（数字化期刊），1999年第10期，http://www.chinainfo.gov.cn/periodical/zgxxdb/zgxxdb99/zgxx9910/991002.htm（2000/4/17）   


---

## js原版
如何开始
- 鼠标单击小恐龙开始

达到500分后

- 开启黑夜模式

有一个小bug
- 当地面、云朵明显变快之后，小恐龙的脚步并没有明显变化

## 我们的程序

如何开始
- 通过键盘监听**回车键**的输入从而开始/结束

关于黑夜模式
- 砍掉？？？ 
  > 答：砍掉。没时间了。

